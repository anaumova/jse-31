package ru.tsc.anaumova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.request.ServerAboutRequest;
import ru.tsc.anaumova.tm.dto.request.ServerVersionRequest;
import ru.tsc.anaumova.tm.dto.response.ServerAboutResponse;
import ru.tsc.anaumova.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}