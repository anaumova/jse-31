package ru.tsc.anaumova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.anaumova.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.anaumova.tm.api.repository.ICommandRepository;
import ru.tsc.anaumova.tm.api.repository.IProjectRepository;
import ru.tsc.anaumova.tm.api.repository.ITaskRepository;
import ru.tsc.anaumova.tm.api.repository.IUserRepository;
import ru.tsc.anaumova.tm.api.service.*;
import ru.tsc.anaumova.tm.command.AbstractCommand;
import ru.tsc.anaumova.tm.command.system.*;
import ru.tsc.anaumova.tm.dto.request.ServerAboutRequest;
import ru.tsc.anaumova.tm.dto.request.ServerVersionRequest;
import ru.tsc.anaumova.tm.endpoint.SystemEndpoint;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.exception.system.UnknownArgumentException;
import ru.tsc.anaumova.tm.exception.system.UnknownCommandException;
import ru.tsc.anaumova.tm.model.User;
import ru.tsc.anaumova.tm.repository.CommandRepository;
import ru.tsc.anaumova.tm.repository.ProjectRepository;
import ru.tsc.anaumova.tm.repository.TaskRepository;
import ru.tsc.anaumova.tm.repository.UserRepository;
import ru.tsc.anaumova.tm.service.*;
import ru.tsc.anaumova.tm.util.SystemUtil;
import ru.tsc.anaumova.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.anaumova.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void initDemoData() {
        @NotNull final User usualUser = userService.create("user", "user", "user@user.ru");
        @NotNull final User adminUser = userService.create("admin", "admin", Role.ADMIN);
        projectService.create(usualUser.getId(), "Project C", "Description C").setStatus(Status.IN_PROGRESS);
        projectService.create(usualUser.getId(), "Project A", "Description A").setStatus(Status.COMPLETED);
        projectService.create(usualUser.getId(), "Project D", "Description D").setStatus(Status.IN_PROGRESS);
        projectService.create(adminUser.getId(), "Project B", "Description B").setStatus(Status.NOT_STARTED);
        taskService.create(usualUser.getId(), "Task F", "Description F").setStatus(Status.IN_PROGRESS);
        taskService.create(adminUser.getId(), "Task E", "Description E").setStatus(Status.NOT_STARTED);
        taskService.create(adminUser.getId(), "Task G", "Description G").setStatus(Status.NOT_STARTED);
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) new ExitCommand().execute();
        prepareStartup();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
        }
    }

    public void processCommand(@NotNull final String command) {
        processCommand(command, true);
    }

    public void processCommand(@NotNull final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new UnknownArgumentException(arg);
        abstractCommand.execute();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}