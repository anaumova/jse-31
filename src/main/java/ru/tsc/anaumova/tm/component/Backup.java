package ru.tsc.anaumova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.command.data.DataBackupLoadCommand;
import ru.tsc.anaumova.tm.command.data.DataBackupSaveCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    private void load() {
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    private void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

}